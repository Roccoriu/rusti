mod util;

use util::window;

fn main() {
    pollster::block_on(window::open());
}